package com.week_4.tests;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.week_4.beans.ProductBean;
import com.week_4.pages.CartScreen;
import com.week_4.pages.HomeScreen;
import com.week_4.pages.ProductDetailsScreen;
import com.week_4.pages.ProductListScreen;

public class FlipkartTest extends WebDriverTestCase {
	
	@Test
	public void testProductAddedToCart() {
		HomeScreen home = new HomeScreen();
		home.launchMobApp();
		//home.waitForScreenToLoad();
		home.closeLoginScreen();
		home.searchProduct("Apple iPad");
		
		ProductListScreen productList = new ProductListScreen();
		productList.clickOnSortByButton();
		productList.sortBy("Price -- Low to High");
		productList.getListOfProducts();
		ProductBean productBean =productList.selectProductByindex(0);
		
		ProductDetailsScreen prodDetails = new ProductDetailsScreen();
		prodDetails.verifyProductDetails(productBean);
		prodDetails.clickAddToCart();
		prodDetails.clickShoppingCartMenu();
		
		CartScreen cart = new CartScreen();
		cart.verifyProductAddedToCart(productBean);
		
	}

}
