package com.week_4.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductListItem extends QAFWebComponent {
	
	@FindBy(locator = "txt.productname.productlistscreen")
	private QAFWebElement txtProductnameProductlistscreen;
	@FindBy(locator = "txt.productprice.productlistscreen")
	private QAFWebElement txtProductpriceProductlistscreen;
	
	
	public QAFWebElement getTxtProductnameProductlistscreen() {
		return txtProductnameProductlistscreen;
	}

	public QAFWebElement getTxtProductpriceProductlistscreen() {
		return txtProductpriceProductlistscreen;
	}

	public ProductListItem(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

}
