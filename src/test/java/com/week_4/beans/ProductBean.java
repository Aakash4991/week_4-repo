package com.week_4.beans;

public class ProductBean {
	
	String productName;
	String productPrize;
	
	public ProductBean(String productName, String productPrize) {
		this.productName = productName;
		this.productPrize = productPrize;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductPrize() {
		return productPrize;
	}
	public void setProductPrize(String productPrize) {
		this.productPrize = productPrize;
	}
	

	
}
