package com.week_4.utils;

import java.text.NumberFormat;
import java.util.Locale;

public class CommonUtils {
	
	
	/**
	 * method is used to convert the price in the US format i.e 22,222
	 * @param price
	 * @return formatted prize
	 */
	public static String getPriceInUSFormat(String price) {
		price =price.replaceAll("Rs.", "");
		Long num = Long.parseLong(price);
		System.out.println(num);
		String priceInUSFormat = NumberFormat.getNumberInstance(Locale.US).format(num);
		System.out.println("Price in comma format "+priceInUSFormat);
		return priceInUSFormat;
	}

}
