package com.week_4.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeScreen extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "ham.mainmenu.homescreen")
	private QAFWebElement hamMainmenuHomescreen;
	@FindBy(locator = "lnk.electronics.homescreen")
	private QAFWebElement lnkElectronicsHomescreen;
	@FindBy(locator = "txt.searchbox.homescreen")
	private QAFWebElement txtSearchboxHomescreen;
	@FindBy(locator = "btn.allow.homescreen")
	private QAFWebElement btnAllowHomeScreen;
	@FindBy(locator = "btn.skip.homescreen")
	private QAFWebElement btnSkipHomeScreen;
	@FindBy(locator = "txt.searchbox2.homescreen")
	private QAFWebElement txtSearchbox2Homescreen;
	@FindBy(locator = "txt.firstproduct.homescreen")
	private QAFWebElement txtFirstproductHomescreen;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getBtnskiphomescreen() {
		return btnSkipHomeScreen;
	}

	public QAFWebElement getHamMainmenuHomescreen() {
		return hamMainmenuHomescreen;
	}

	public QAFWebElement getLnkElectronicsHomescreen() {
		return lnkElectronicsHomescreen;
	}

	public QAFWebElement getTxtSearchboxHomescreen() {
		return txtSearchboxHomescreen;
	}

	public QAFWebElement getBtnallowhomescreen() {
		return btnAllowHomeScreen;
	}

	public QAFWebElement getTxtSearchbox2Homescreen() {
		return txtSearchbox2Homescreen;
	}
	
	public QAFWebElement getTxtFirstproductHomescreen() {
		return txtFirstproductHomescreen;
	}

	/**
	 *  LaunchMobApp
	 */
	public void launchMobApp() {
		driver.get("/");
	}
	
	/**
	 * Wait for Screen to load before performing any action and click on allow button
	 */
	public void waitForScreenToLoad() {
		btnAllowHomeScreen.waitForVisible(100000);
		btnAllowHomeScreen.click();
		System.out.println("Clicked on allow button");
	}
	
	/**
	 * Close the login screen
	 */
	public void closeLoginScreen() {
		//driver.switchTo().defaultContent();
		btnSkipHomeScreen.waitForVisible();
		btnSkipHomeScreen.click();
	}

	/**
	 * Search product based on product Name passed and methods also provide product bean object
	 * @param productName
	 */
	public void searchProduct(String productName) {
		txtSearchboxHomescreen.waitForVisible(100000);
		txtSearchboxHomescreen.click();
		txtSearchbox2Homescreen.sendKeys(productName);
		txtFirstproductHomescreen.waitForVisible();
		System.out.println("First product selected is "+txtFirstproductHomescreen.getText());
		txtFirstproductHomescreen.click();
		//waitForScreenToLoad();
	}



}
