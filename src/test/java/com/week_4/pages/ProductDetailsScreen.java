package com.week_4.pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.week_4.beans.ProductBean;
import com.week_4.utils.CommonUtils;

public class ProductDetailsScreen extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "txt.productname.productdetailsscreen")
	private QAFWebElement txtProductnameProductdetailsscreen;
	@FindBy(locator = "txt.productprice.productdetailsscreen")
	private QAFWebElement txtProductpriceProductdetailsscreen;
	@FindBy(locator = "btn.addtocart.productdetailsscreen")
	private QAFWebElement btnAddtocartProductdetailsscreen;
	@FindBy(locator = "btn.gotocart.productdetailsscreen")
	private QAFWebElement btnGotocartProductdetailsscreen;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getTxtProductnameProductdetailsscreen() {
		return txtProductnameProductdetailsscreen;
	}

	public QAFWebElement getTxtProductpriceProductdetailsscreen() {
		return txtProductpriceProductdetailsscreen;
	}

	public QAFWebElement getBtnAddtocartProductdetailsscreen() {
		return btnAddtocartProductdetailsscreen;
	}

	public QAFWebElement getBtnGotocartProductdetailsscreen() {
		return btnGotocartProductdetailsscreen;
	}

	
	/**
	 * method will verify product details with respect to product bean object
	 * @param productBean
	 */
	public void verifyProductDetails(ProductBean productBean) {
		System.out.println("Product bean content is Name:- "+productBean.getProductName()+" and Prize:- "+productBean.getProductPrize());
		Validator.verifyThat(getTxtProductnameProductdetailsscreen().getText(), Matchers.containsString(productBean.getProductName()));
		String formattedPrize = CommonUtils.getPriceInUSFormat(productBean.getProductPrize());
		Validator.verifyThat(getTxtProductpriceProductdetailsscreen().getText(), Matchers.containsString(formattedPrize));
	}
	
	/**
	 * method will click on add to cart button
	 */
	public void clickAddToCart() {
		btnAddtocartProductdetailsscreen.click();
	}
	
	/**
	 * method will click on shopping cart menu on the menu bar 
	 */
	public void clickShoppingCartMenu() {
		btnGotocartProductdetailsscreen.click();
	}
}
