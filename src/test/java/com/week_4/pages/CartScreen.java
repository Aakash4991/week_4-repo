package com.week_4.pages;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.week_4.beans.ProductBean;
import com.week_4.utils.CommonUtils;

public class CartScreen extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "txt.productname.cartscreen")
	private QAFWebElement txtProductnameCartscreen;
	@FindBy(locator = "txt.productprice.cartscreen")
	private QAFWebElement txtProductpriceCartscreen;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getTxtProductnameCartscreen() {
		return txtProductnameCartscreen;
	}

	public QAFWebElement getTxtProductpriceCartscreen() {
		return txtProductpriceCartscreen;
	}
	
	
	/**
	 * method verifies whether the product is added to cart or not.
	 * @param productBean
	 */
	public void verifyProductAddedToCart(ProductBean productBean) {
		System.out.println("Product bean content is Name:- "+productBean.getProductName()+" and Prize:- "+productBean.getProductPrize());
		QAFExtendedWebElement prodName = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
											.getString("txt.productname.cartscreen"),productBean.getProductName()));
		
		prodName.waitForPresent();
		prodName.assertVisible("Product is present on Cart Page");
/*		boolean isProductPresent = prodName.isPresent();
		System.out.println("Product is present - "+isProductPresent);
		Reporter.log("Product is present - "+isProductPresent);*/
		
		
		String formattedPrize = CommonUtils.getPriceInUSFormat(productBean.getProductPrize());
		QAFExtendedWebElement prodPrize = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
				.getString("txt.productprice.cartscreen"),formattedPrize));
		prodPrize.assertVisible("Product prize is present for the added product ");
/*		boolean isProductPrizePresent = prodPrize.isDisplayed();
		System.out.println("Product prize is present - "+isProductPrizePresent);
		Reporter.log("Product prize is present - "+isProductPrizePresent);*/
	}

}
