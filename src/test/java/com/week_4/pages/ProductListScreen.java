package com.week_4.pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.week_4.beans.ProductBean;
import com.week_4.components.ProductListItem;

/**
 * @author Aakash.Sharma
 *
 */
public class ProductListScreen extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "btn.sortby.productlistscreen")
	private QAFWebElement btnSortbyProductlistscreen;
	@FindBy(locator = "txt.sortbyparameter.productlistscreen")
	private QAFWebElement txtSortByParameterProductlistscreen;
	@FindBy(locator = "lst.productlist.productlistscreen")
	private List<ProductListItem> lstProductlistProductlistscreen;
	@FindBy(locator="txt.availabilitystatus.productlistscreen") private QAFWebElement txtAvailabilitystatusProductlistscreen;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getbtnSortbyProductlistscreen() {
		return btnSortbyProductlistscreen;
	}

	public QAFWebElement getBtnSortbyProductlistscreen() {
		return btnSortbyProductlistscreen;
	}

	public QAFWebElement getTxtSortByParameterProductlistscreen() {
		return txtSortByParameterProductlistscreen;
	}

	public List<ProductListItem> getLstProductlistProductlistscreen() {
		return lstProductlistProductlistscreen;
	}
	
	public QAFWebElement getTxtAvailabilitystatusProductlistscreen(){
		return txtAvailabilitystatusProductlistscreen;
	}

	
	/**
	 * method selects the required sortby option
	 * @param sortByParameter
	 */
	public void sortBy(String sortByParameter) {
		QAFExtendedWebElement sort = new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle()
				.getString("txt.sortbyparameter.productlistscreen"),sortByParameter));
		sort.click();
	}
	
	/**
	 * sort the list with price->low to high
	 */
	public void clickOnSortByButton() {
		btnSortbyProductlistscreen.click();
		//txtPricelowtohighsortProductlistscreen.click();
	}
	
	/**
	 * method to scroll screen
	 */
	public void scrollScreen() {
/*		TouchActions action = new TouchActions(driver);
		action.scroll(element, 10, 100);
		action.perform();*/	
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250)");
		
	}

	/**
	 * Select product at particular index and return the product bean object for verification
	 * @param index
	 * @return product bean
	 */
	public ProductBean selectProductByindex(int index) {
		ProductListItem productListItem = getLstProductlistProductlistscreen().get(index);
		String productName = productListItem.getTxtProductnameProductlistscreen().getText();
		String productPrize = productListItem.getTxtProductpriceProductlistscreen().getText();
		ProductBean productBean = new ProductBean(productName, productPrize);
		productListItem.click();
		return productBean;
	}

	/**
	 * method will provide all the products in the productlist
	 */
	public void getListOfProducts() {
		System.out.println("Product List Size is "+getLstProductlistProductlistscreen().size());
		Reporter.log("Product List Size is "+getLstProductlistProductlistscreen().size());
		for (int i=0;i<getLstProductlistProductlistscreen().size()-1;i++) {
			System.out.println("In loop :- "+i );
			ProductListItem prodListItem = getLstProductlistProductlistscreen().get(i);
			if(prodListItem.getTxtProductnameProductlistscreen().isDisplayed()) {
				String productName = prodListItem.getTxtProductnameProductlistscreen().getText();
				String productPrize = prodListItem.getTxtProductpriceProductlistscreen().getText();
				System.out.println("Name of product at index "+i+" is "+productName);
				System.out.println("Prize of product at index "+i+" is "+productPrize);
				Reporter.log("Name of product at index "+i+" is "+productName);
				Reporter.log("Prize of product at index "+i+" is "+productPrize);
			}
			else {
				System.out.println("Product is not visible");
			}
			
		}	
	}


}
